//freeze service
// import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
// import { GridModule, FreezeService, SelectionService } from '@syncfusion/ej2-angular-grids';
// import { AppComponent } from './app.component';

// /**
//  * Module
//  */
// @NgModule({
//     imports: [
//         BrowserModule,
//         GridModule
//     ],
//     declarations: [AppComponent],
//     bootstrap: [AppComponent],
//     providers: [FreezeService, SelectionService]
// })
// export class AppModule { }


//workking one
import { NgModule,ViewChild } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TreeGridModule } from '@syncfusion/ej2-angular-treegrid';
// import { PageService, SortService, FilterService,EditService,ToolbarService } from '@syncfusion/ej2-angular-treegrid';
// import { ResizeService, ExcelExportService, PdfExportService, ContextMenuService } from '@syncfusion/ej2-angular-treegrid';

import { FilterService, PageService, SortService, ResizeService, ColumnMenuService, ReorderService, RowDDService, EditService , ToolbarService  } from '@syncfusion/ej2-angular-treegrid';

import { AppComponent } from './app.component';
import {ButtonModule} from '@syncfusion/ej2-angular-buttons';
import { DropDownListAllModule } from '@syncfusion/ej2-angular-dropdowns';
import { ColumnChooserService } from '@syncfusion/ej2-angular-grids';

/**
 * Module
 */
@NgModule({
    imports: [
        BrowserModule,
        TreeGridModule,
        ButtonModule,
        DropDownListAllModule,
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    providers: [PageService,
                SortService,
                FilterService,
                //EditService,
                ResizeService,
                //ExcelExportService,
                //PdfExportService, ContextMenuService,
                //ToolbarService,
                 ColumnChooserService,
                 ReorderService,
                 RowDDService,
                 EditService,
                 ToolbarService ]
})
export class AppModule { }