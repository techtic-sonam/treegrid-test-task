import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { sortData } from './jsontreegriddata';
import { PageService, ToolbarItems, ToolbarService, TreeGrid } from '@syncfusion/ej2-angular-treegrid';

import { TreeGridComponent} from '@syncfusion/ej2-angular-treegrid';
import { getValue, isNullOrUndefined } from '@syncfusion/ej2-base';
import { BeforeOpenCloseEventArgs } from '@syncfusion/ej2-inputs';
import { MenuEventArgs } from '@syncfusion/ej2-navigations';
import { ColumnChooserService, SortEventArgs } from '@syncfusion/ej2-angular-grids';
import { CheckBoxComponent } from '@syncfusion/ej2-angular-buttons';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [ PageService, ColumnChooserService, ToolbarService ]
})
export class AppComponent implements OnInit {
  public data: Object[] = [];
  filterSettings: Object;
  pageSettings: Object;

  public sortSettings: Object;


  @ViewChild(TreeGridComponent, {static: true}) treegrid: TreeGridComponent;

  orderName: boolean;
  category: boolean;
  orderDate: boolean;
  units: boolean;
  price: boolean;

  public editSettings: Object;
  public toolbar:string[];
  public taskidrules: Object;
    public tasknamerules: Object;
    public startdaterules: Object;
    public durationrules: Object;
    public numericParams: Object;

  //public contextMenuItems: Object;
  ngOnInit(): void {
      // this.data = sampleData;
      this.data = sortData;
      this.filterSettings = { type: 'Menu'};
      this.pageSettings = { pageSize: 10 };
      this.editSettings ={ allowEditing: true, allowAdding: true, allowDeleting: true, mode:"Batch", newRowPosition: 'Below'};
      this.toolbar = ['Add', 'Delete', 'Update', 'Cancel'];

      this.taskidrules = { required: true , number: true};
      this.tasknamerules = { required: true};
      this.startdaterules = { required: true};
      this.numericParams = {required: true};
      this.sortSettings =  { columns: [{ field: 'Category', direction: 'Ascending'  },
                                         { field: 'orderName', direction: 'Ascending' }]
                             }

       this.filterSettings = { type: 'FilterBar', hierarchyMode: 'Parent', mode: 'Immediate' };
  }

  // public onClick1(e: MouseEvent): void {
  //   console.log(e);
  //   if (e.target) {
  //       console.log('checked',this.treegrid);

  //       this.treegrid.sortByColumn('taskName', 'Ascending', true);
  //   } else {
  //       //this.treegrid.removeSortColumn('orderName');
  //       this.treegrid.grid.removeSortColumn('taskName');
  //   }

  // }


  public onClick1(e): void {
    // this.ordrName = 'order name';
    // //console.log(this.ordrName,"this.ordrName");
    // this.data = sortData;
    // console.log(this.data,"sortData");
    // this.treegrid.refreshColumns();
    // console.log(e.target.checked);
    let isActive = e.target.checked;
    if (isActive) {
        console.log("checked");
        this.orderName = true;
        this.treegrid.sortByColumn('orderName', 'Ascending', true);
    } else {
        console.log("unchecked");
        this.orderName = false;
        this.treegrid.grid.removeSortColumn('orderName');
    }

}
public onClick2(e): void {
    console.log(e.target.checked);
    let isActive = e.target.checked;
    if (isActive) {
      this.category = true;
        this.treegrid.sortByColumn('Category', 'Ascending', true);
    } else {
      this.category = false;
        this.treegrid.grid.removeSortColumn('Category');
    }

}
public onClick3(e): void {
    console.log(e.target.checked);
    let isActive = e.target.checked;
    if (isActive) {
        this.orderDate = true;
        this.treegrid.sortByColumn('orderDate', 'Ascending', true);
    } else {
        this.orderDate = false;
        this.treegrid.grid.removeSortColumn('orderDate');
    }

}
public onClick4(e): void {
    console.log(e.target.checked);
    let isActive = e.target.checked;
    if (isActive) {
        this.units = true;
        this.treegrid.sortByColumn('units', 'Ascending', true);
    } else {
      this.units = false;
        this.treegrid.grid.removeSortColumn('units');
    }

}

public sort (args: SortEventArgs ): void {
    if (args.requestType === 'sorting') {
        for (let columns of this.treegrid.getColumns()) {
            for (let sortcolumns of this.treegrid.sortSettings.columns) {
                if (sortcolumns.field === columns.field) {
                    this.check(sortcolumns.field, true); break;
                } else {
                    this.check(columns.field, false);
                }
            }
        }
    }

}
public check(field: string, state: boolean): void {
  console.log("state",state);
    switch (field) {
        case 'orderName':
            this.orderName = state; break;
        case 'Category':
            this.category = state; break;
        case 'orderDate':
            this.orderDate = state; break;
        case 'units':
            this.units = state; break;
        case 'price':
            this.price = state; break;
    }
}

  // contextMenuOpen (arg?: BeforeOpenCloseEventArgs): void {
  //     console.log('click here',arg);
  //     let elem: Element = arg.event.target as Element;
  //     let row: Element = elem.closest('.e-row');
  //     let uid: string = row && row.getAttribute('data-uid');
  //     let items: Array<HTMLElement> = [].slice.call(document.querySelectorAll('.e-menu-item'));
  //     for (let i: number = 0; i < items.length; i++) {
  //       items[i].setAttribute('style','display: none;');
  //     }
  //     if (elem.closest('.e-row')) {
  //       if ( isNullOrUndefined(uid) ||
  //         isNullOrUndefined(getValue('hasChildRecords', this.treegrid.grid.getRowObjectFromUID(uid).data))) {
  //         arg.cancel = true;
  //       } else {
  //         let flag: boolean = getValue('expanded', this.treegrid.grid.getRowObjectFromUID(uid).data);
  //         let val: string = flag ? 'none' : 'block';
  //         document.querySelectorAll('li#expandrow')[0].setAttribute('style', 'display: ' + val + ';');
  //         val = !flag ? 'none' : 'block';
  //         document.querySelectorAll('li#collapserow')[0].setAttribute('style', 'display: ' + val + ';');
  //       }
  //     } else {
  //       let len = this.treegrid.element.querySelectorAll('.e-treegridexpand').length;
  //       if (len !== 0) {
  //          document.querySelectorAll('li#collapseall')[0].setAttribute('style', 'display: block;');
  //       } else {
  //         document.querySelectorAll('li#expandall')[0].setAttribute('style', 'display: block;');
  //       }
  //     }
  // }
  // contextMenuClick (args?: MenuEventArgs): void {
  //     if (args.item.id === 'collapserow') {
  //       this.treegrid.collapseRow(this.treegrid.getSelectedRows()[0] as HTMLTableRowElement, this.treegrid.getSelectedRecords()[0]);
  //     } else if (args.item.id === 'expandrow') {
  //       this.treegrid.expandRow(this.treegrid.getSelectedRows()[0] as HTMLTableRowElement, this.treegrid.getSelectedRecords()[0]);
  //     } else if (args.item.id === 'collapseall') {
  //       this.treegrid.collapseAll();
  //     } else if (args.item.id === 'expandall') {
  //       this.treegrid.expandAll();
  //     }

  // }
}